package org.contract.record;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.field.Byte;
import org.contract.field.Double;
import org.contract.field.Float;
import org.contract.field.Long;
import org.contract.field.Short;
import org.contract.field.*;
import org.contract.field.array.Dynamic;
import org.contract.field.base.IndexWire;
import org.contract.field.base.WireI;
import org.contract.type.Desc;
import org.contract.type.Type;
import org.contract.utils.AlignOf;
import org.contract.utils.Pointer;

import java.util.Arrays;

public class Struct extends IndexWire {
    public static Meta Meta = new Meta();
    public static class Meta {
        final Type<Struct> rec = new Type<Struct>(Struct::new) {
            @Override
            public long offsetOf() {
                return 0;
            }

            @Override
            public Type<Struct> offsetOf(long offset) {
                return this;
            }

            @Override
            public long sizeOf(long eCount) {
                return 0;
            }

            @Override
            public Desc describe() {
                return Desc.var("x".getBytes());
            }

            @Override
            public long alignOf() {
                return 0;
            }
        };
        final Type<Double> d = Double.Meta.type(0);
        final Type<Int> i = Int.Meta.type(8);
        final Type<Maybe<Struct>> nextM = Maybe.Meta.type(rec, 16);
        final Type<Maybe<Struct>> prevM = Maybe.Meta.type(rec, 24);
        final Type<Bool> b = Bool.Meta.type(32);
        final Type<Short> s = Short.Meta.type(34);
        final Type<Long> l = Long.Meta.type(40);
        final Type<Ref<Dynamic<Char>>> rdc = Ref.Meta.type(Dynamic.Meta.type(Char.Meta.type()), 48);
        final Type<Byte> bte = Byte.Meta.type(56);
        final Type<Float> f = Float.Meta.type(60);
        final Type<Record> p = Record.Meta.type(64);
        final Type<Ref<Dynamic<Record>>> dataArr = Ref.Meta.type(Dynamic.Meta.type(Record.Meta.type()), 128);
        final Type<Ref<Record>> dataR = Ref.Meta.type(Record.Meta.type(), 136);

        public Type<Struct> Type = type();
        public Type<Struct> type() {
            return type(0);
        }
        public Type<Struct> type(final long offset) {
            return new Type<Struct>(Struct::new) {
                long off = offset;

                @Override
                public long offsetOf() {
                    return off;
                }

                @Override
                public Type<Struct> offsetOf(long offset) {
                    return this;
                }

                @Override
                public long sizeOf(final long count) {
                    long r = 0;

                    r = AlignOf.apply(r, d.alignOf()) + d.sizeOf();
                    r = AlignOf.apply(r, i.alignOf()) + i.sizeOf();
                    r = AlignOf.apply(r, nextM.alignOf()) + nextM.sizeOf();
                    r = AlignOf.apply(r, prevM.alignOf()) + prevM.sizeOf();
                    r = AlignOf.apply(r, b.alignOf()) + b.sizeOf();
                    r = AlignOf.apply(r, s.alignOf()) + s.sizeOf();
                    r = AlignOf.apply(r, l.alignOf()) + l.sizeOf();
                    r = AlignOf.apply(r, rdc.alignOf()) + rdc.sizeOf();
                    r = AlignOf.apply(r, bte.alignOf()) + bte.sizeOf();
                    r = AlignOf.apply(r, f.alignOf()) + f.sizeOf();
                    r = AlignOf.apply(r, p.alignOf()) + p.sizeOf();
                    r = AlignOf.apply(r, dataArr.alignOf()) + dataArr.sizeOf();
                    r = AlignOf.apply(r, dataR.alignOf()) + dataR.sizeOf();
                    
                    return count * (AlignOf.apply(r, alignOf()));
                }

                @Override
                public Desc describe() {
                    return
                            Desc.recursive("x".getBytes(),
                                    Desc.record(Arrays.asList(
                                            Desc.field("d".getBytes(), d.offsetOf(), d.describe()),
                                            Desc.field("i".getBytes(), i.offsetOf(), i.describe()),
                                            Desc.field("nextM".getBytes(), nextM.offsetOf(), nextM.describe()),
                                            Desc.field("prevM".getBytes(), prevM.offsetOf(), prevM.describe()),
                                            Desc.field("b".getBytes(), b.offsetOf(), b.describe()),
                                            Desc.field("s".getBytes(), s.offsetOf(), s.describe()),
                                            Desc.field("l".getBytes(), l.offsetOf(), l.describe()),
                                            Desc.field("rdc".getBytes(), rdc.offsetOf(), rdc.describe()),
                                            Desc.field("bte".getBytes(), bte.offsetOf(), bte.describe()),
                                            Desc.field("f".getBytes(), f.offsetOf(), f.describe()),
                                            Desc.field("p".getBytes(), p.offsetOf(), p.describe()),
                                            Desc.field("dataArr".getBytes(), dataArr.offsetOf(), dataArr.describe()),
                                            Desc.field("dataR".getBytes(), dataR.offsetOf(), dataR.describe())
                                            ))
                                    );
                }

                @Override
                public long alignOf() {
                    long r = 0;

                    r = Math.max(r, d.alignOf());
                    r = Math.max(r, i.alignOf());
                    r = Math.max(r, nextM.alignOf());
                    r = Math.max(r, prevM.alignOf());
                    r = Math.max(r, b.alignOf());
                    r = Math.max(r, s.alignOf());
                    r = Math.max(r, l.alignOf());
                    r = Math.max(r, rdc.alignOf());
                    r = Math.max(r, bte.alignOf());
                    r = Math.max(r, f.alignOf());
                    r = Math.max(r, p.alignOf());
                    r = Math.max(r, dataArr.alignOf());
                    r = Math.max(r, dataR.alignOf());

                    return r;
                }
            };
        }
    }

    private Double d;   // {8:8-15}
    private Int i; // {4:16-19}
    private Maybe<Struct> nextM;   // {8:24-31}
    private Maybe<Struct> prevM; // {8:32-39}
    private Bool b; // {1:40-40}
    private Short s; // {2:42-43}
    private Long l; // {8:48-55}
    private Ref<Dynamic<Char>> rdc; // {8:56-64}
    private Byte bte; // {1:65-65}
    private Float f; // {1:68-71}
    private Record p; // {24:72-95}
    private Ref<Dynamic<Record>> dataArr; // {8:96-103}
    private Ref<Record> dataR; // {8:104-111}

    public double getDouble() {
        return d.get();
    }

    public Struct setDouble(double d) {
        this.d.set(d);
        return this;
    }

    public int getInt() {
        return i.get();
    }

    public Struct setInt(int i) { this.i.set(i); return this; }

    public Maybe<Struct> getNextMaybe() {
        return nextM;
    }

    public Maybe<Struct> getPrevMaybe() { return prevM; }

    public Struct setBool(boolean b) { this.b.set(b); return this; }

    public boolean getBool() { return this.b.get(); }

    public Struct setShort(short s) { this.s.set(s); return this; }

    public short getShort() { return this.s.get(); }

    public Struct setLong(long l) { this.l.set(l); return this; }

    public long getLong() { return this.l.get(); }

    public Ref<Dynamic<Char>> getRDC() { return rdc; }

    public Struct setByte(byte b) { this.bte.set(b); return this; }

    public byte getByte() { return this.bte.get(); }

    public Struct setFloat(float f) { this.f.set(f); return this; }

    public float getFloat() { return this.f.get(); }

    public Ref<Dynamic<Record>> getDataArr() { return dataArr; }

    public Ref<Record> getDataR() { return dataR; }

    public Struct(final UnsafeBuffer unsafeBuffer) {
       this(unsafeBuffer, Pointer.make(8L));
    }

    public Struct(final UnsafeBuffer unsafeBuffer, final Pointer<java.lang.Long> base) {
        super(unsafeBuffer, base);
        init(Meta);
    }

    @Override
    public WireI wire(UnsafeBuffer unsafeBuffer) {
        d.wire(unsafeBuffer);
        i.wire(unsafeBuffer);
        nextM.wire(unsafeBuffer);
        prevM.wire(unsafeBuffer);
        b.wire(unsafeBuffer);
        s.wire(unsafeBuffer);
        l.wire(unsafeBuffer);
        rdc.wire(unsafeBuffer);
        bte.wire(unsafeBuffer);
        f.wire(unsafeBuffer);
        p.wire(unsafeBuffer);
        dataArr.wire(unsafeBuffer);
        dataR.wire(unsafeBuffer);

        return super.wire(unsafeBuffer);
    }

    public void init(final Meta meta) {
        write(index());
        d = meta.d.reify(wire(), write(index() + meta.d.offsetOf()));
        i = meta.i.reify(wire(), write(index() + meta.i.offsetOf()));
        nextM = meta.nextM.reify(wire(), write(index() + meta.nextM.offsetOf()));
        prevM = meta.prevM.reify(wire(), write(index() + meta.prevM.offsetOf()));
        b = meta.b.reify(wire(), write(index() + meta.b.offsetOf()));
        s = meta.s.reify(wire(), write(index() + meta.s.offsetOf()));
        l = meta.l.reify(wire(), write(index() + meta.l.offsetOf()));
        rdc = meta.rdc.reify(wire(), write(index() + meta.rdc.offsetOf()));
        bte = meta.bte.reify(wire(), write(index() + meta.bte.offsetOf()));
        f = meta.f.reify(wire(), write(index() + meta.f.offsetOf()));
        p = meta.p.reify(wire(), write(index() + meta.p.offsetOf()));
        dataArr = meta.dataArr.reify(wire(), write(index() + meta.dataArr.offsetOf()));
        dataR = meta.dataR.reify(wire(), write(index() + meta.dataR.offsetOf()));
    }
}
