package org.contract.record;

import org.agrona.concurrent.UnsafeBuffer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RecordTest {

    Record record;

    @BeforeEach
    void setUp() {
        record = new Record(new UnsafeBuffer(ByteBuffer.allocateDirect(4096)));
    }

    @AfterEach
    void tearDown() {
    }

    @Tag("org.contract.record.RecordTest.testSizeOf")
    @Test
    void testSizeOf() {
        assertEquals(8, record.index());
        assertEquals(64, Record.Meta.type().sizeOf());
        assertEquals(68, record.write().get());
    }
}
