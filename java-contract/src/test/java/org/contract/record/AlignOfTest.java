package org.contract.record;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.contract.field.*;
import org.contract.field.Double;
import org.contract.field.Long;
import org.contract.field.Short;
import org.contract.field.array.Dynamic;
import org.contract.type.Type;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AlignOfTest {
    Type<Double> d;
    Type<Int> i;
    Type<Maybe<Struct>> nextM;
    Type<Maybe<Struct>> prevM;
    Type<Bool> b;
    Type<Short> s;
    Type<Long> l;
    Type<Ref<Dynamic<Char>>> str;

    @Tag("AlignOf.basics")
    @Test
    void basics() {
        assertEquals(8, d.alignOf());
        assertEquals(4, i.alignOf());
        assertEquals(8, nextM.alignOf());
        assertEquals(8, prevM.alignOf());
        assertEquals(1, b.alignOf());
        assertEquals(2, s.alignOf());
        assertEquals(8, l.alignOf());
        assertEquals(8, str.alignOf());

        long r = 0;
        r = Math.max(r, d.alignOf());
        assertEquals(8, r);
        r = Math.max(r, i.alignOf());
        assertEquals(8, r);
        r = Math.max(r, 8);
        assertEquals(8, r);
        r = Math.max(r, 8);
        assertEquals(8, r);
        r = Math.max(r, b.alignOf());
        assertEquals(8, r);
        r = Math.max(r, s.alignOf());
        assertEquals(8, r);
        r = Math.max(r, l.alignOf());
        assertEquals(8, r);
        r = Math.max(r, str.alignOf());
        assertEquals(8, r);

        assertEquals(8, Struct.Meta.type().alignOf());
    }

    @BeforeEach
    void setUp() {
        d = Double.Meta.type(0);
        i = Int.Meta.type(8);
        nextM = Maybe.Meta.type(Struct.Meta.Type, 16);
        prevM = Maybe.Meta.type(Struct.Meta.Type, 24);
        b = Bool.Meta.type(32);
        s = org.contract.field.Short.Meta.type(34);
        l = Long.Meta.type(40);
        str = Ref.Meta.type(Dynamic.Meta.type(Char.Meta.Type), 48);
    }
}
