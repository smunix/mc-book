package org.contract.versions.expand;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.exception.IllegalConversion;
import org.contract.field.Char;
import org.contract.field.Double;
import org.contract.field.Int;
import org.contract.field.base.IndexWire;
import org.contract.field.base.WireI;
import org.contract.type.Desc;
import org.contract.type.Type;
import org.contract.utils.AlignOf;
import org.contract.utils.Pointer;
import org.contract.utils.functional.Thunk1;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

public class A2 extends IndexWire {
    public static class Nested extends IndexWire {
        public static Meta Meta = new Meta();
        public static class Meta {
            public Type<Int> i = Int.Meta.type(0);
            public Type<Char> c = Char.Meta.type(4);
            public Type<Nested> type(final long offset) {
                return new Type<Nested>(Nested::new) {
                    long off = offset;

                    @Override
                    public long offsetOf() {
                        return off;
                    }

                    @Override
                    public Type<Nested> offsetOf(long offset) {
                        off = offset;
                        return this;
                    }

                    @Override
                    public long sizeOf(long eCount) {
                        long r = 0;
                        r = AlignOf.apply(r, i.alignOf()) + i.sizeOf();
                        r = AlignOf.apply(r, c.alignOf()) + c.sizeOf();
                        return eCount * AlignOf.apply(r, alignOf());
                    }

                    @Override
                    public Desc describe() {
                        return Desc.record(Arrays.asList(
                                Desc.field("i", i.offsetOf(), i.describe()),
                                Desc.field("c", c.offsetOf(), c.describe())
                        ));
                    }

                    @Override
                    public long alignOf() {
                        long r = 0;
                        r = Math.max(r, i.alignOf());
                        r = Math.max(r, c.alignOf());
                        return r;
                    }

                    @Override
                    public Boolean from(long off, Desc desc) {
                        Thunk1<Boolean, Desc> err = (d -> {
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            d.show(new PrintStream(baos));
                            new IllegalConversion(baos.toString()).printStackTrace(System.err);
                            return false;
                        });
                        Thunk1<Boolean, Boolean> ret = (b -> {
                            this.offsetOf(off);
                            return b;
                        });
                        return desc.match(
                                () -> false,
                                (prim -> err.apply(prim)),
                                (var -> err.apply(var)),
                                (farr -> err.apply(farr)),
                                (arr -> err.apply(arr)),
                                (variant -> err.apply(variant)),
                                (struct -> {
                                    boolean r = true;
                                    r &= desc.with("i", (o, desc1) -> i.from(o, desc1));
                                    r &= desc.with("c", (o, desc1) -> c.from(o, desc1));
                                    return r;
                                }),
                                (nat -> err.apply(nat)),
                                (app -> err.apply(app)),
                                (recursive -> err.apply(recursive)),
                                (fn -> err.apply(fn))
                        );
                    }
                };
            }
        }

        public Int i;
        public Char c;

        public Nested(UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
            super(unsafeBuffer, position);
            init(Meta);
        }

        @Override
        public WireI wire(UnsafeBuffer unsafeBuffer) {
            i.wire(unsafeBuffer);
            c.wire(unsafeBuffer);
            return super.wire(unsafeBuffer);
        }

        public void init(Meta meta) {
            write(index());
            i = meta.i.reify(wire(), write(index() + meta.i.offsetOf()));
            c = meta.c.reify(wire(), write(index() + meta.c.offsetOf()));
        }
    }

    public static Meta Meta = new Meta();
    public static class Meta {
        public final Nested.Meta nMeta = new Nested.Meta();

        public final Type<Char> c = Char.Meta.type(0);
        public final Type<Int> i = Int.Meta.type(4);
        public final Type<Nested> n = nMeta.type(8);
        public final Type<Double> d = Double.Meta.type(16);

        public Type<A2> Type = type();
        public Type<A2> type() {
            return type(0);
        }
        public Type<A2> type(final long offset) {
            return new Type<A2>(A2::new) {
                long off = offset;

                @Override
                public long offsetOf() {
                    return offset;
                }

                @Override
                public Type<A2> offsetOf(long offset) {
                    off = offset;
                    return this;
                }

                @Override
                public long sizeOf(long eCount) {
                    long r = 0;
                    r = AlignOf.apply(r, c.alignOf()) + c.sizeOf();
                    r = AlignOf.apply(r, i.alignOf()) + i.sizeOf();
                    r = AlignOf.apply(r, n.alignOf()) + n.sizeOf();
                    r = AlignOf.apply(r, d.alignOf()) + d.sizeOf();
                    return eCount * AlignOf.apply(r, alignOf());
                }

                @Override
                public Desc describe() {
                    return Desc.record(Arrays.asList(
                            Desc.field("c", c.offsetOf(), c.describe()),
                            Desc.field("i", i.offsetOf(), i.describe()),
                            Desc.field("n", n.offsetOf(), n.describe()),
                            Desc.field("d", d.offsetOf(), d.describe())
                    ));
                }

                @Override
                public long alignOf() {
                    long r = 0;
                    r = Math.max(r, c.alignOf());
                    r = Math.max(r, i.alignOf());
                    r = Math.max(r, n.alignOf());
                    r = Math.max(r, d.alignOf());
                    return r;
                }

                @Override
                public Boolean from(long off, Desc desc) {
                    offsetOf(off);

                    boolean r = true;
                    r &= desc.with("c", (o, desc1) -> c.from(o, desc1));
                    r &= desc.with("i", (o, desc1) -> i.from(o, desc1));
                    r &= desc.with("n", (o, desc1) -> n.from(o, desc1));
                    r &= desc.with("d", (o, desc1) -> d.from(o, desc1));
                    return r;
                }
            };
        }
    }
    // fields
    public Char c;
    public Int i;
    public Nested n;
    public Double d;

    public A2(UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
        super(unsafeBuffer, position);
        init(Meta);
    }

    @Override
    public WireI wire(UnsafeBuffer unsafeBuffer) {
        c.wire(unsafeBuffer);
        i.wire(unsafeBuffer);
        n.wire(unsafeBuffer);
        d.wire(unsafeBuffer);
        return super.wire(unsafeBuffer);
    }

    public void init(Meta meta) {
        write(index());
        c = meta.c.reify(wire(), write(index() + meta.c.offsetOf()));
        i = meta.i.reify(wire(), write(index() + meta.i.offsetOf()));
        n = meta.n.reify(wire(), write(index() + meta.n.offsetOf()));
        n.init(meta.nMeta);
        d = meta.d.reify(wire(), write(index() + meta.d.offsetOf()));
    }
}
