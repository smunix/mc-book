package org.contract.utils.functional;

public interface Function {
    default <R, A0, A1> Thunk1<Thunk0<R>, A0> curry(final Thunk1<R, A0> thunk1) {
        return (a0) -> () -> thunk1.apply(a0);
    }
    default <R, A0, A1> Thunk1<Thunk1<Thunk0<R>, A1>, A0> curry(final Thunk2<R, A0, A1> thunk2) {
        return (a0) -> (a1) -> () -> thunk2.apply(a0, a1);
    }
    default <R, A0, A1, A2> Thunk1<Thunk1<Thunk1<Thunk0<R>, A2>, A1>, A0> curry(final Thunk3<R, A0, A1, A2> thunk3) {
        return (a0) -> (a1) -> (a2) -> () -> thunk3.apply(a0, a1, a2);
    }
    default <R, A0, A1, A2, A3> Thunk1<Thunk1<Thunk1<Thunk1<Thunk0<R>, A3>, A2>, A1>, A0> curry(final Thunk4<R, A0, A1, A2, A3> thunk4) {
        return (a0) -> (a1) -> (a2) -> (a3) -> () -> thunk4.apply(a0, a1, a2, a3);
    }
    default <R, A0, A1, A2, A3, A4> Thunk1<Thunk1<Thunk1<Thunk1<Thunk1<Thunk0<R>, A4>, A3>, A2>, A1>, A0> curry(final Thunk5<R, A0, A1, A2, A3, A4> thunk5) {
        return (a0) -> (a1) -> (a2) -> (a3) -> (a4) -> () -> thunk5.apply(a0, a1, a2, a3, a4);
    }

}
