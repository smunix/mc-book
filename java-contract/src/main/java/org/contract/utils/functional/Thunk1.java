package org.contract.utils.functional;

@FunctionalInterface
public interface Thunk1<R, A0> {
    R apply(A0 a0);
}
