package org.contract.utils.functional;

@FunctionalInterface
public interface Thunk2<R, A0, A1> {
    R apply(A0 a0, A1 a1);
}
