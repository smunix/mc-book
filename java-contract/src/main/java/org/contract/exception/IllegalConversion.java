package org.contract.exception;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class IllegalConversion extends Throwable {
    private final String msgError;

    public IllegalConversion(String msgError) {
        this.msgError = msgError;
    }

    @Override
    public String toString() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        ps.print(getClass().getCanonicalName() + ":" + msgError);
        return baos.toString();
    }
}
