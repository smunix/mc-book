package org.contract.field.array;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.field.base.IndexI;
import org.contract.field.base.IndexWire;
import org.contract.type.Desc;
import org.contract.type.Type;
import org.contract.utils.AlignOf;
import org.contract.utils.Pointer;
import org.contract.utils.functional.Thunk2;

import java.util.Arrays;

public class Capacity<E extends IndexI> extends IndexWire {
    public static Meta Meta = new Meta();
    public static class Meta {
        public <F extends IndexI> Type type(final Type<F> eType, final long eCount) {
            return type(0, eType, eCount);
        }
        public <F extends IndexI> Type<Capacity> type(final long offset, final Type<F> eType, final long eCount) {
            return new Type<Capacity>((unsafeBuffer, base) -> new Capacity(unsafeBuffer, base)) {
                long off = offset;

                @Override
                public long offsetOf() {
                    return off;
                }

                @Override
                public Type<Capacity> offsetOf(long offset) {
                    off = offset;
                    return null;
                }

                @Override
                public long sizeOf(final long count) {
                    return count * AlignOf.apply(eType.sizeOf(eCount), eType.alignOf());
                }

                @Override
                public Desc describe() {
                    return Desc.app(Desc.fn(Arrays.asList("a".getBytes(), "n".getBytes()), Desc.prim("carray".getBytes(), Desc.arr(Desc.var("a".getBytes()), Desc.var("n".getBytes())))), Arrays.asList(eType.describe()));
                }

                @Override
                public long alignOf() {
                    return eType.alignOf();
                }

            };
        }
        public Desc describe() {
            return Desc.fn(Arrays.asList("a".getBytes(), "n".getBytes()), Desc.prim("carray".getBytes(), Desc.arr(Desc.var("a".getBytes()), Desc.var("n".getBytes()))));
        }
    }

    public Capacity(UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
        super(unsafeBuffer, position);
    }
}
