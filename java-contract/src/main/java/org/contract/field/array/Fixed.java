package org.contract.field.array;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.field.base.IndexI;
import org.contract.field.base.IndexWire;
import org.contract.type.Desc;
import org.contract.type.Type;
import org.contract.utils.AlignOf;
import org.contract.utils.Pointer;
import org.contract.utils.compile.Long.Static;
import org.contract.utils.functional.Thunk2;

import java.util.Arrays;

public class Fixed<E extends IndexI, S extends Static> extends IndexWire {
    private S sz;
    private Type<E> eType;

    public static Meta Meta = new Meta();
    public static class Meta {
        public <E extends IndexI, S extends Static> Type<Fixed<E, S>> type(final Type<E> eType, final S sz) {
            return type(0, eType, sz);
        }
        public <E extends IndexI, S extends Static> Type<Fixed<E, S>> type(final long offset, final Type<E> eType, final S sz) {
            return new Type<Fixed<E, S>>((unsafeBuffer, base) -> new Fixed<>(sz, eType, unsafeBuffer, base)) {
                long off = offset;

                @Override
                public long offsetOf() {
                    return off;
                }

                @Override
                public Type<Fixed<E, S>> offsetOf(long offset) {
                    off = offset;
                    return this;
                }

                @Override
                public long sizeOf(final long count) {
                    return count * AlignOf.apply(eType.sizeOf(sz.get()), eType.alignOf());
                }

                @Override
                public Desc describe() {
                    return Desc.app(Desc.fn(Arrays.asList("a".getBytes(), "n".getBytes()), Desc.arr(Desc.var("a".getBytes()), Desc.var("n".getBytes()))), Arrays.asList(eType.describe(), Desc.nat(sz.get())));
                }

                @Override
                public long alignOf() {
                    return eType.alignOf();
                }
            };
        }
    }

    public Fixed(final S sz, final Type<E> eType, UnsafeBuffer unsafeBuffer, Pointer<Long> position) {
        super(unsafeBuffer, position);
        this.sz = sz;
        this.eType = eType;
        init(Meta);
    }
    public void init(Meta meta) {
        write(index() + meta.type(eType, sz).sizeOf());
    }
    public void init(final long offset, Meta meta) {
        write(index() + meta.type(offset, eType, sz).sizeOf());
    }
}
