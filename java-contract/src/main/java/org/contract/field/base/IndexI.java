package org.contract.field.base;

import org.contract.utils.Pointer;

public interface IndexI {
    long index();
    void index(final long position);
    void index(final int position);

    Pointer<Long> write();
    Pointer<Long> write(Pointer<Long> pointer);

    default Pointer<Long> write(final long position) {
        return write().set(position);
    }
    default Pointer<Long> write(final int position) {
        return write().set((long)position);
    }
}
