package org.contract.field.base;

import org.agrona.concurrent.UnsafeBuffer;
import org.contract.utils.Pointer;

public class IndexWire extends Index implements WireI {
    UnsafeBuffer unsafeBuffer;

    public IndexWire(final UnsafeBuffer unsafeBuffer, final Pointer<Long> position) {
        super(position);
        this.unsafeBuffer = unsafeBuffer;
    }

    @Override
    public UnsafeBuffer wire() {
        return unsafeBuffer;
    }

    @Override
    public WireI wire(UnsafeBuffer unsafeBuffer) {
        this.unsafeBuffer = unsafeBuffer;
        return this;
    }

    @Override
    public Pointer<Long> write() {
        return write;
    }

    @Override
    public Pointer<Long> write(Pointer<Long> pointer) {
        write = pointer;
        return write;
    }
}
