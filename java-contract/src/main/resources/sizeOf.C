#include <iostream>
#define log(x) do { std::cout << x << std::endl; } while(0)

struct A {
  double d;
  int i;
};

struct B {
  int i;
  char c;
};

struct C {
  char c;
  double d;
  int i;
};

struct D {
  char c;
  int i;
};

template<class T>
struct maybe {
  long l;
};

template<class T>
struct ref {
  long l;
};

struct _ {};

template<class T, size_t N>
using fixed = T[N];

template<class T>
struct array {
  size_t len;
  T data[0];
};

struct data {
  char c;
  double d;
  int i;
  fixed<int, 10> fi;
};

template<class T>
struct typed {
  T _;
};

struct base {
  double d;
  int i;
  maybe<_> nextM;
  maybe<_> prevM;
  bool b;
  short s;
  long l;
  ref<_> rdc;
  char c;
  float f;
  data p;
  ref<array<data>> rdM;
  ref<data> rd;
  ref<typed<data>> rtd;
};

auto main() -> int {
  log("sizeof(A)=" << sizeof(A));
  log("sizeof(B)=" << sizeof(B));
  log("sizeof(C)=" << sizeof(C));
  log("sizeof(D)=" << sizeof(D));

  log("sizeof(data)=" << sizeof(data));

  log("offsetof(struct data, c)=" << (int)offsetof(struct data, c));
  log("offsetof(struct data, d)=" << (int)offsetof(struct data, d));
  log("offsetof(struct data, i)=" << (int)offsetof(struct data, i));
  log("offsetof(struct data, fi)=" << (int)offsetof(struct data, fi));

  log("sizeof(base)=" << sizeof(base));
  log("sizeof(fixed<base, 1>)=" << sizeof(fixed<base, 1>));
  log("sizeof(fixed<fixed<base, 1>, 1>)=" << sizeof(fixed<fixed<base, 1>, 1>));
  log("sizeof(fixed<fixed<base, 2>, 1>)=" << sizeof(fixed<fixed<base, 2>, 1>));
  log("sizeof(fixed<fixed<base, 2>, 2>)=" << sizeof(fixed<fixed<base, 2>, 2>));
  log("sizeof(fixed<fixed<base, 1>, 2>)=" << sizeof(fixed<fixed<base, 1>, 2>));
  log("sizeof(fixed<fixed<base, 10>, 1>)=" << sizeof(fixed<fixed<base, 10>, 1>));

  log("offsetof(struct base, d)=" << (int)offsetof(struct base, d));
  log("offsetof(struct base, i)=" << (int)offsetof(struct base, i));
  log("offsetof(struct base, nextM)=" << (int)offsetof(struct base, nextM));
  log("offsetof(struct base, prevM)=" << (int)offsetof(struct base, prevM));
  log("offsetof(struct base, b)=" << (int)offsetof(struct base, b));
  log("offsetof(struct base, s)=" << (int)offsetof(struct base, s));
  log("offsetof(struct base, l)=" << (int)offsetof(struct base, l));
  log("offsetof(struct base, rdc)=" << (int)offsetof(struct base, rdc));
  log("offsetof(struct base, c)=" << (int)offsetof(struct base, c));
  log("offsetof(struct base, f)=" << (int)offsetof(struct base, f));
  log("offsetof(struct base, p)=" << (int)offsetof(struct base, p));
  log("offsetof(struct base, rdM)=" << (int)offsetof(struct base, rdM));
  log("offsetof(struct base, rd)=" << (int)offsetof(struct base, rd));
  log("offsetof(struct base, rtd)=" << (int)offsetof(struct base, rtd));

  return 0;
}
