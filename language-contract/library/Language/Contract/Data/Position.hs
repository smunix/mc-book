module Language.Contract.Data.Position ( Position(..)
                                 , prettyPosition
                                 , incPosition
                                 , retPosition
                                 , incOffset
                                 , initPosition
                                 , maxPosition
                                 , minPosition
                                 --
                                 , Span(..)
                                 , prettySpan
                                 , subsetOf
                                 , Spanned(..)
                                 , unSpan
                                 , (#)
                                 --
                                 , Located(..)
                                 ) where

import GHC.Generics (Generic)

import Control.DeepSeq (NFData)
import Data.Data (Data)
import Data.Typeable (Typeable)

import Data.List.NonEmpty (NonEmpty(..))
import Data.Monoid as Mon
import Data.Semigroup as Sem

-- | A position in the source file.
data Position where
  Position :: { offset :: {-# UNPACK #-} !Int -- ^ absolute offset the source file.
              , row :: {-# UNPACK #-} !Int -- ^ row (line) in the source file.
              , col :: {-# UNPACK #-} !Int -- ^ column in the source file.
              } -> Position
  NoPosition :: Position
  deriving (Eq, Ord, Data, Typeable, Generic, NFData)

instance Show Position where
  showsPrec _ NoPosition = showString "NoPosition"
  showsPrec p (Position{..}) = showParen (p >= 11)
                               ( showString "Position"
                               . showString " " . showsPrec 11 offset
                               . showString " " . showsPrec 11 row
                               . showString " " . showsPrec 11 col
                               )

prettyPosition :: Position -> String
prettyPosition NoPosition = "???"
prettyPosition (Position _ r c) = show r <> ":" <> show c

comparePositionWith :: (Int -> Int -> Bool) -> Position -> Position -> Position
comparePositionWith _ NoPosition p = p
comparePositionWith _ p NoPosition = p
comparePositionWith f ap1@(Position p1 _ _) ap2@(Position p2 _ _) = if f p1 p2 then ap1 else ap2

{-# INLINE maxPosition #-}
maxPosition :: Position -> Position -> Position
maxPosition = comparePositionWith (>)

{-# INLINE minPosition #-}
minPosition :: Position -> Position -> Position
minPosition = comparePositionWith (<)

-- | Starting position in a file.
initPosition :: Position
initPosition = Position 0 1 0

-- | Advance column a certain number of times
incPosition :: Position -> Int -> Position
incPosition NoPosition _ = NoPosition
incPosition p@Position{..} i = p{ offset = offset + i, col = col + i }

-- | Advance column a certain number of times
incOffset :: Position -> Int -> Position
incOffset NoPosition _ = NoPosition
incOffset p@Position{..} i = p{ offset = offset + i }

-- | Advance to the next line.
retPosition :: Position -> Position
retPosition NoPosition = NoPosition
retPosition p@Position{..} = p{ offset= offset + 1, row = row + 1, col = 1 }

-- | Spans represent a contiguous region of code, delimited by two 'Position's. The endpoints are inclusive.
data Span where
  Span :: { lo, hi :: !Position } -> Span
  deriving (Eq, Ord, Data, Typeable, Generic, NFData)

instance Show Span where
  showsPrec p (Span l h) = showParen (p >=11)
                           ( showString "Span"
                           . showString " " . showsPrec 11 l
                           . showString " " . showsPrec 11 h
                           )

instance Sem.Semigroup Span where
  {-# INLINE (<>) #-}
  Span l1 h1 <> Span l2 h2 = Span (minPosition l1 l2) (maxPosition h1 h2)

instance Mon.Monoid Span where
  mempty = Span NoPosition NoPosition
  {-# INLINE mempty #-}
  mappend = (Sem.<>)

prettySpan :: Span -> String
prettySpan (Span l h) = show l <> " - " <> show h

-- | Convenience function lifting '<>' to work on all 'Located' things
{-# INLINE (#) #-}
(#) :: (Located a, Located b) => a -> b -> Span
left # right = spanOf left <> spanOf right

subsetOf :: Span -> Span -> Bool
subsetOf (Span l1 h1) (Span l2 h2) = minPosition l1 l2 == l1 && maxPosition h1 h2 == h2

-- | A "tagging" of something with a 'Span' that describes its extent.
data Spanned a where
  Spanned :: a
          -> {-# UNPACK #-} !Span
          -> Spanned a
  deriving (Eq, Ord, Data, Typeable, Generic, NFData, Show)

unSpan :: Spanned a -> a
unSpan (Spanned a _) = a

instance Functor Spanned where
  {-# INLINE fmap #-}
  fmap f (Spanned a s) = Spanned (f a) s

instance Applicative Spanned where
  {-# INLINE pure #-}
  pure a = Spanned a mempty
  {-# INLINE (<*>) #-}
  (Spanned f s1) <*> (Spanned a s2) = Spanned (f a) (s1 Sem.<> s2)

instance Monad Spanned where
  return = pure
  (Spanned a s) >>= f = Spanned a' (s Sem.<> s')
    where
      Spanned a' s' = f a

class Located a where
  spanOf :: a -> Span

instance Located (Spanned a) where
  spanOf (Spanned _ s) = s

instance Located Span where
  spanOf = id

instance (Located a) => Located (Maybe a) where
  spanOf = foldMap spanOf

instance (Located a) => Located [a] where
  spanOf = foldMap spanOf

instance (Located a) => Located (NonEmpty a) where
  spanOf = foldMap spanOf

 
