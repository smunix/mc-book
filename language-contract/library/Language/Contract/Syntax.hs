module Language.Contract.Syntax ( module Language.Contract.Syntax.Token
                          , module Language.Contract.Syntax.Ast
                          ) where

import Language.Contract.Syntax.Token
import Language.Contract.Syntax.Ast
