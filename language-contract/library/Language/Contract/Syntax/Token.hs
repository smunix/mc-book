module Language.Contract.Syntax.Token ( Token (..)
                                , Space(..)
                                , Language(..)
                                , Delim(..)
                                , Literal(..)
                                ) where

import GHC.Generics (Generic)

import Control.DeepSeq (NFData)
import Data.Data (Data)
import Data.Typeable (Typeable)
import Data.Maybe (fromMaybe)

import Language.Contract.Data

-- | A general token representation
data Token where
  NoToken    :: Token
  -- Single character expression-operator symbols
  Equal      :: Token -- ^ @=@ token
  Less       :: Token -- ^ @<@ token
  Pipe       :: Token -- ^ @|@ token
  -- Multi character expression-operator symbols
  -- Structural symbols
  At         :: Token -- ^ @\@@ token
  Question   :: Token -- ^ @?@ token
  Dot        :: Token -- ^ @.@ token
  Comma      :: Token -- ^ @,@ token
  Colon      :: Token -- ^ @:@ token
  SemiColon  :: Token -- ^ @;@ token
  ColonColon :: Token -- ^ @::@ token
  -- Name components
  Ident      :: Ident -- ^ Identifier
             -> Token
  Space      :: Space
             -> Name  -- ^ whitespace
             -> Token
  Doc        :: String
             -> Token
  -- Delimiters
  Open       :: !Delim
             -> Token
  Close      :: !Delim
             -> Token
  -- Literals
  Literal    :: Literal
             -> (Maybe Name)
             -> Token
  -- Markers
  Preamble :: Literal
           -> Token
  Lang       :: Token
  Language   :: Language
             -> Token
  Eof        :: Token
  deriving (Eq, Ord, Data, Typeable, Generic, NFData, Show)

data Language where
  Java :: Language
  Cpp :: Language
  Python :: Language
  deriving (Eq, Ord, Data, Typeable, Generic, NFData, Show)

data Delim where
  Paren :: Delim
  Bracket :: Delim
  Brace :: Delim
  NoDelim :: Delim
  deriving (Eq, Ord, Data, Typeable, Generic, NFData, Show)

data Literal where
  Byte :: Name -- ^ byte
       -> Literal
  Char :: Name -- ^ character
       -> Literal
  Integer :: Name -- ^ integral literal (could have type @i8@, @i16@, @i32@, @int@, @u128@, etc.)
          -> Literal
  Float :: Name -- ^ floating point literal (could have type @f32@, @f64@, etc.)
        -> Literal
  String :: Name
         -> Literal
  RawString :: Name
            -> !Int
            -> Literal
  ByteString :: Name
             -> Literal
  ByteStrings :: [Name]
             -> Literal
  RawByteString :: Name
                -> !Int
                -> Literal
  deriving (Eq, Ord, Data, Typeable, Generic, NFData, Show)

data Space where
  Whitespace :: Space
  Comment :: Space
  deriving (Eq, Ord, Data, Typeable, Generic, NFData, Show)
