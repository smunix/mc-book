module Language.Contract.Parser.ContractMonad ( ContractState(..)
                                  , ContractMonad(..)
                                  , ParseFail(..)
                                  -- execution
                                  , run
                                  , run'
                                  -- state
                                  , getState
                                  , setState
                                  , modifyState
                                  -- position
                                  , getPreviousPosition
                                  , setPreviousPosition
                                  , getCurrentPosition
                                  , getPosition
                                  , setPosition
                                  -- input stream
                                  , getInput
                                  , setInput
                                  -- token manipulation
                                  , swapToken
                                  , pushToken
                                  , popToken
                                  -- error
                                  , contractError
                                  -- start code
                                  , setStartCode
                                  , getStartCode
                                  -- file path
                                  , setFilePath
                                  , getFilePath
                                  ) where

import qualified Language.Contract.Data as Data
import qualified Language.Contract.Syntax as Syn

import qualified Control.Monad.Fail as Fail
import Control.Exception (Exception)
import Data.Maybe (listToMaybe)
import Data.Typeable (Typeable)

-- | State that the Lexer and the parser do share
data ContractState where
  ContractState :: { currentPosition   :: !Data.Position           -- ^ position at current input location
             , currentInput      :: !Data.InputStream        -- ^ the current input
             , previousPosition :: !Data.Position            -- ^ position at previous input location
             , pushedTokens      :: [Data.Spanned Syn.Token] -- ^ tokens manually pushed by the user
             , swapFunction      :: Syn.Token -> Syn.Token   -- ^ function to swap token
             , filePath          :: FilePath
             , startCode         :: !Int                     -- ^ Start Code used on the Lexer
             } -> ContractState

-- | Parsing and Lexing Monad.
newtype ContractMonad a = ContractMonad { unContractMonad :: forall r . ContractState -- ^ State being passed along.
                                        -> (a -> ContractState -> r) -- ^ Succeed wih a value and current parser state.
                                        -> (String -> Data.Position -> r) -- ^ Fail with a string and current position in the input stream.
                                        -> r -- ^ Final output
                            }

instance Functor ContractMonad where
  fmap f (ContractMonad m) = ContractMonad (\ !st successFn failFn -> m st (successFn . f) failFn)

instance Applicative ContractMonad where
  pure a = ContractMonad (\ st successFn _ -> successFn a st)
  (ContractMonad f) <*> (ContractMonad k) = ContractMonad $ \ !st successFn failFn ->
    let
      successFn' f' st' = k st' (successFn . f') failFn
    in
      f st successFn' failFn

instance Monad ContractMonad where
  return = pure
  (ContractMonad m) >>= k = ContractMonad $ \ !st successFn failFn ->
    let
      successFn' a' st' =
        let
          ContractMonad k' = (k a')
        in
          k' st' successFn failFn
    in
      m st successFn' failFn

instance Fail.MonadFail ContractMonad where
  fail msg = ContractMonad $ \ !st _ failFn -> failFn msg (currentPosition st)

data ParseFail where
  ParseFail :: Data.Position
            -> String
            -> ParseFail
  deriving (Eq, Typeable)

instance Show ParseFail where
  showsPrec p (ParseFail errPos errMsg) = showParen (p >= 11) (showString err)
    where
      err = unwords [ "parse failure at", Data.prettyPosition errPos, "(" <> errMsg <> ")"]
instance Exception ParseFail

-- | Execute the given ContractMonad parser on the supplied input stream at the given start position.
-- Return either the position of an error and the error message, or the value parsed
run :: ContractMonad a
     -> Data.InputStream
     -> Data.Position
     -> Either ParseFail a
run = run' id

-- | A generalized version of the ContractMonad execution, whcih expects a swapping function that lets you hot-swap
-- a token that was just lexed before it gets passed to the Parser.
run' :: (Syn.Token -> Syn.Token)
      -> ContractMonad a
      -> Data.InputStream
      -> Data.Position
      -> Either ParseFail a
run' swapFn (ContractMonad contractM) is pos = contractM initState (\ a -> const $ Right a) (\errMsg errPos -> Left $ ParseFail errPos errMsg)
  where
    initState = ContractState { currentPosition   = pos
                        , currentInput      = is
                        , previousPosition  = pos
                        , pushedTokens      = []
                        , swapFunction      = swapFn
                        , filePath          = "<buffer>"
                        , startCode         = 0
                        }
-- | Swap a token using the swap function
swapToken :: Syn.Token -> ContractMonad Syn.Token
swapToken t = ContractMonad $ \ !st@ContractState{..} successFn _ -> successFn (swapFunction $! t) st

-- | Extract the state stored in the Parser
getState :: ContractMonad ContractState
getState = ContractMonad $ \ !st successFn _ -> successFn st st

-- | Override the state stored in the Parser
setState :: ContractState -> ContractMonad ()
setState st = ContractMonad $ \ !_ successFn _ -> successFn () st

modifyState :: (ContractState -> ContractState)
            -> ContractMonad ()
modifyState fn = ContractMonad $ \ !st successFn _ -> successFn () (fn $! st)

getPreviousPosition :: ContractMonad Data.Position
getPreviousPosition = previousPosition <$> getState

setPreviousPosition :: Data.Position -> ContractMonad ()
setPreviousPosition p = modifyState $ \ !st -> st{ previousPosition = p }

getCurrentPosition = getPosition

getPosition :: ContractMonad Data.Position
getPosition = currentPosition <$> getState

setPosition :: Data.Position -> ContractMonad ()
setPosition p = modifyState $ \ !st -> st{ currentPosition = p }

getInput :: ContractMonad Data.InputStream
getInput = currentInput <$> getState

setInput :: Data.InputStream -> ContractMonad ()
setInput is = modifyState $ \ !st -> st{ currentInput = is }

getStartCode :: ContractMonad Int
getStartCode = startCode <$> getState

setStartCode :: Int -> ContractMonad ()
setStartCode sc = modifyState $ \ !st -> st{ startCode = sc }

pushToken :: Data.Spanned Syn.Token -> ContractMonad ()
pushToken tok = modifyState $ \ !st@ContractState{..} -> st{ pushedTokens = tok : pushedTokens }

popToken :: ContractMonad (Maybe (Data.Spanned Syn.Token))
popToken = ContractMonad $ \ !st@ContractState{..} successFn _ -> successFn (listToMaybe pushedTokens) st{ pushedTokens = drop 1 pushedTokens }

setFilePath :: FilePath -> ContractMonad ()
setFilePath fp = modifyState $ \ !st -> st{ filePath = fp }

getFilePath :: ContractMonad FilePath
getFilePath = ContractMonad $ \ !st successFn _ -> successFn (filePath $! st) st

contractError :: (Show b) => b -> ContractMonad a
contractError b = Fail.fail ("syntax error: the synbol `" <> show b <> "` does not belong to here")
