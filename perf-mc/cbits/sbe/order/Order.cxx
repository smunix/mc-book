#include <sbe/sbe.H>
#include <sbe/order/VarAsciiEncoding.h>
#include <sbe/order/MessageHeader.h>
#include <sbe/order/Order.h>

template<class T>
using Ptr = T*;

namespace sbe {
  auto initHdr(Ptr<MessageHeader> h, Ptr<Wire<Order>> w, uint64_t off, uint64_t len) -> uint64_t {
    constexpr int messageHeaderVersion = 0;

    h->wrap(w->buff(), off, messageHeaderVersion, len)
      .blockLength(Order::sbeBlockLength())
      .templateId(Order::sbeTemplateId())
      .schemaId(Order::sbeSchemaId())
      .version(Order::sbeSchemaVersion())
      ;
    return h->encodedLength();
  }

  auto initOrder(Ptr<Order> o, Ptr<Wire<Order>> w, uint64_t off, uint64_t len) -> uint64_t {
    o->wrapForEncode(w->buff(), off, len);
    return o->encodedLength();
  }
}

extern "C" {
  using namespace sbe;

  auto sbeNewMessageHeader(Ptr<Wire<Order>> w) -> Ptr<MessageHeader> {
    dblog("create new message header");
    auto h = new MessageHeader{};
    initHdr(h, w, 0, w->size());
    return h;
  }

  auto sbeDelMessageHeader(Ptr<MessageHeader> o) -> void {
    dblog("delete messageHeader");
    delete o;
  }

  auto sbeMessageHeaderEncodedLength(Ptr<MessageHeader> h) -> uint64_t {
    dblog("messageHeader encoded length");
    return h->encodedLength();
  }

  auto sbeNewOrder(Ptr<Wire<Order>> w, Ptr<MessageHeader> h) -> Ptr<Order> {
    dblog("create new order");
    auto o = new Order{};
    initOrder(o, w, sbeMessageHeaderEncodedLength(h), w->size());
    return o;
  }

  auto sbeDelOrder(Ptr<Order> o) -> void {
    dblog("delete order");
    delete o;
  }

  auto sbeOrderEncodedLength(Ptr<Order> o) -> uint64_t {
    dblog("order encoded length");
    return o->encodedLength();
  }

  auto sbeNewWire() -> Ptr<Wire<Order>> {
    dblog("create new wire");
    return new Wire<Order>{};
  }
  auto sbeDelWire(Ptr<Wire<Order>> w) -> void {
    dblog("delete wire");
    delete w;
  }
  auto sbeSizeWire(Ptr<Wire<Order>> w) -> uint64_t {
    dblog("get Size");
    return w->size();
  }
  auto sbeOrderAccumQtySet(Ptr<Order> o, int32_t v) -> void {
    dblog("set accumQty");
    o->accumQty(v);
  }
  auto sbeOrderAccumQtyGet(Ptr<Order> o, void (*fn) (int32_t)) -> void {
    dblog("get accumQty");
    fn(o->accumQty());
  }
  auto sbeOrderPriceSet(Ptr<Order> o, double v) -> void {
    dblog("set price");
    o->price(v);
  }
  auto sbeOrderPriceGet(Ptr<Order> o, void (*fn) (double)) -> void {
    dblog("get price");
    fn(o->price());
  }
  auto sbeOrderSymbolSet(Ptr<Order> o, char const* str, uint64_t len) -> void {
    dblog("set symbol");
    try {
      o->putSymbol(str, len);
    } catch (std::runtime_error const& ex) {
      dblog(ex.what());
    }
  }
  auto sbeOrderSymbolGet(Ptr<Order> o, char *dst, uint64_t len) -> uint64_t {
    dblog("get symbol");
    try {
      return o->getSymbol(dst, len);
    } catch (std::runtime_error const& ex) {
      dblog(ex.what());
    }
    return 0;
  }
  auto sbeOrderTextSet(Ptr<Order> o, char const* str, uint64_t len) -> void {
    dblog("set text");
    try {
      o->putText(str, len);
    } catch (std::runtime_error const& ex) {
      dblog(ex.what());
    }
  }
  auto sbeOrderTextGet(Ptr<Order> o, char *dst, uint64_t len) -> void {
    dblog("get text");
    try {
      o->getText(dst, len);
    } catch (std::runtime_error const& ex) {
      dblog(ex.what());
    }
  }
}
