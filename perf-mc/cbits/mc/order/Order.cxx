#include <mc/mc.H>

using namespace mc;

namespace mc {
  struct Order {
    uint32_t ui32;
    data<int32_t>               _accumQty;
    data<double>                _price;
    data<ref<dyn::arr<char>>>   _symbol;
    data<ref<dyn::arr<char>>>   _text;
    data<ref<dyn::arr<Order>>>  _children;

    auto accumQty() -> decltype(*_accumQty)& { return *_accumQty; }
    auto price()    -> decltype(*_price)&    { return *_price;    }
    auto symbol()   -> decltype(*_symbol)&   { return *_symbol;   }
    auto text()     -> decltype(*_text)&     { return *_text;     }
  };
}

template<class T>
using Ptr = T*;

extern "C" {
  using namespace mc;

  auto mcNewWire() -> Ptr<Wire<Order>> {
    dblog("create new wire");
    return new Wire<Order>{};
  }
  auto mcDelWire(Ptr<Wire<Order>> w) -> void {
    dblog("delete wire");
    delete w;
  }
  auto mcObjWire(Ptr<Wire<Order>> w) -> Ptr<Order> {
    dblog("get Order");
    return &w->get();
  }
  auto mcSizeWire(Ptr<Wire<Order>> w) -> uint64_t {
    dblog("get Size");
    return w->size();
  }
  auto mcOrderAccumQtySet(Ptr<Order> o, int32_t v) -> void {
    dblog("set accumQty");
    o->accumQty() = v;
  }
  auto mcOrderAccumQtyGet(Ptr<Order> o, void(*fn)(int32_t)) -> void {
    dblog("get accumQty");
    return fn(o->accumQty());
  }
  auto mcOrderPriceSet(Ptr<Order> o, double v) -> void {
    dblog("set price");
    o->price() = v;
  }
  auto mcOrderPriceGet(Ptr<Order> o, void(*fn)(double)) -> void {
    dblog("get price");
    return fn(o->price());
  }
  auto mcOrderSymbolSet(Ptr<Wire<Order>> w, char const* str, uint64_t len) -> void {
    dblog("set symbol");
    w->assign(w->get().symbol()).put(str, len);
  }
  auto mcOrderSymbolGet(Ptr<Order> o, char** str, uint64_t* len) -> void {
    dblog("get symbol");
    (*o->symbol()).array(*str, *len);
  }
  auto mcOrderTextSet(Ptr<Wire<Order>> w, char const* str, uint64_t len) -> void {
    dblog("set text");
    w->assign(w->get().text()).put(str, len);
  }
  auto mcOrderTextGet(Ptr<Order> o, char** str, uint64_t* len) -> void {
    dblog("get text");
    (*o->text()).array(*str, *len);
  }
  auto mcOrderUI32Set(Ptr<Order> o, uint32_t v) -> void {
    dblog("set ui32");
    o->ui32 = v;
  }
}
