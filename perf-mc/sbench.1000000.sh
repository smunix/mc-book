#! /usr/bin/env sh
s bench --ba='--regress cpuTime:iters --output=report.1000000.cpuTime:iters.html --time-limit 10 --match glob "*/*/1000000*/*"' &> report.1000000.cpuTime:iters.log
