Config {cIn = "reports/report.1000.cpuTime:iters.csv", cOutPrefix = Just "1000", cOutDir = Just "reports/show-bench-1000"}
Creating chart [(Mean)] at "reports/show-bench-1000/1000-coeff-Mean.txt"
(Mean)
Benchmark      mc/1000 Orders(ms) sbe/1000 Orders - mc/1000 Orders(%)
-------------- ------------------ -----------------------------------
read/accumQty                3.06                               +0.16
read/price                   3.04                               +0.83
write/symbol                 2.69 [31m                             +34.46[0m
write/text                   2.66 [31m                             +78.71[0m
read/text                    2.64 [31m                             +76.62[0m
read/symbol                  2.64 [31m                             +76.96[0m
write/accumQty               2.62                               +0.22
write/price                  2.60                               +1.06

Creating chart [(Mean)] at "reports/show-bench-1000/1000-coeff-Mean.svg"
