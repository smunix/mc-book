Config {cIn = "reports/report.100000.cpuTime:iters.csv", cOutPrefix = Just "100000", cOutDir = Just "reports/show-bench-100000"}
Creating chart [(Mean)] at "reports/show-bench-100000/100000-coeff-Mean.txt"
(Mean)
Benchmark      mc/100000 Orders(ms) sbe/100000 Orders - mc/100000 Orders(%)
-------------- -------------------- ---------------------------------------
read/price                   575.90                                   +0.87
read/accumQty                572.59                                   +2.01
write/symbol                 538.77                                   +0.42
write/text                   535.14                                   +1.02
read/text                    533.89                                   +0.68
write/accumQty               533.30                                   +2.81
read/symbol                  532.05                                   +0.88
write/price                  524.24                                   +2.46

Creating chart [(Mean)] at "reports/show-bench-100000/100000-coeff-Mean.svg"
