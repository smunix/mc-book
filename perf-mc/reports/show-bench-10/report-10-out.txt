Config {cIn = "reports/report.10.cpuTime:iters.csv", cOutPrefix = Just "10", cOutDir = Just "reports/show-bench-10"}
Creating chart [(Mean)] at "reports/show-bench-10/10-coeff-Mean.txt"
(Mean)
Benchmark      mc/10 Orders(μs) sbe/10 Orders - mc/10 Orders(%)
-------------- ---------------- -------------------------------
read/price                 6.67                           +1.22
read/accumQty              5.86                           +2.15
write/text                 3.07 [31m                        +563.67[0m
write/symbol               3.04 [31m                        +567.81[0m
read/symbol                2.93 [31m                        +676.54[0m
read/text                  2.89 [31m                        +694.55[0m
write/accumQty             2.61                           -1.65
write/price                2.60                           +2.05

Creating chart [(Mean)] at "reports/show-bench-10/10-coeff-Mean.svg"
