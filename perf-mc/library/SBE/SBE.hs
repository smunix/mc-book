module SBE.SBE ( Wire(..)
               , newWire
               , Wired(..)
               , MessageHeader
               ) where

import GHC.Generics (Generic)
import Control.DeepSeq

import Foreign.Ptr
import Foreign.C
import Foreign.ForeignPtr

import Data.Word

data MessageHeader t

data Wire t where
  Wire :: { wirePtr  :: ForeignPtr (Wire t)
          , wireHdr  :: ForeignPtr (MessageHeader t)
          , wireSize :: IO Word64
          , wireGet  :: ForeignPtr t
          } -> Wire t
  deriving (Generic)
instance (NFData t) => NFData (Wire t) where
  rnf _ = ()

foreign import ccall "sbeNewWire"  sbeNewWire  :: IO (Ptr (Wire t))
foreign import ccall "&sbeDelWire" sbeDelWire  :: FunPtr (Ptr (Wire t) -> IO ())
foreign import ccall "sbeSizeWire" sbeSizeWire :: Ptr (Wire t) -> IO CULong
foreign import ccall "sbeNewMessageHeader" sbeNewMessageHeader :: Ptr (Wire t) -> IO (Ptr (MessageHeader t))
foreign import ccall "&sbeDelMessageHeader" sbeDelMessageHeader :: FunPtr (Ptr (MessageHeader t) -> IO ())

newWire :: (Ptr (Wire t) -> Ptr (MessageHeader t) -> IO (Ptr t)) -> FinalizerPtr t -> IO (Wire t)
newWire newT delT = do
  wireFp <- sbeNewWire >>= newForeignPtr sbeDelWire
  withForeignPtr wireFp $ \wireP -> do
    hdrP <- sbeNewMessageHeader wireP
    hdrFp <- newForeignPtr sbeDelMessageHeader hdrP
    getFp <- newT wireP hdrP  >>= newForeignPtr delT
    return $ Wire { wirePtr = wireFp
                  , wireHdr = hdrFp
                  , wireSize = fromIntegral <$> sbeSizeWire wireP
                  , wireGet = getFp
                  }

class Wired t where
  get  :: Wire t -> t
  size :: Wire t -> IO Word64
