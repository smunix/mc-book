module MC.MC ( Wire(..)
             , newWire
             , Wired(..)
             , Ref(..)
             , DArray(..)
             , FArray(..)
             ) where

import GHC.Generics (Generic)
import Control.DeepSeq

import Foreign.Ptr
import Foreign.C
import Foreign.ForeignPtr

import Data.Word

data Wire t where
  Wire :: { wirePtr :: ForeignPtr (Wire t)
          , wireSize :: IO Word64
          , wireGet :: Ptr t
          } -> Wire t
  deriving (Generic)
instance (NFData t) => NFData (Wire t) where
  rnf w = rnf (wireGet w)

foreign import ccall "mcNewWire"  mcNewWire  :: IO (Ptr (Wire t))
foreign import ccall "&mcDelWire" mcDelWire  :: FunPtr (Ptr (Wire t) -> IO ())
foreign import ccall "mcObjWire"  mcObjWire  :: Ptr (Wire t) -> IO (Ptr t)
foreign import ccall "mcSizeWire" mcSizeWire :: Ptr (Wire t) -> IO CULong

newWire :: IO (Wire t)
newWire = do
  wireFp <- mcNewWire >>= newForeignPtr mcDelWire
  withForeignPtr wireFp $ \wireP -> (>>= return . Wire wireFp (fromIntegral <$> mcSizeWire wireP)) . mcObjWire $ wireP

class Wired t where
  get  :: Wire t -> t
  size :: Wire t -> IO Word64

data Ref t where
  Ref :: { refPtr :: Ptr (Ref t)
         , refUnref :: IO (Maybe (Ptr t))
         } -> Ref t
  deriving (Generic)

data DArray t where
  DArray :: { darrayPtr :: Ptr (DArray t)
            } -> DArray t
  deriving (Generic)

data FArray t where
  FArray :: { farrayPtr :: Ptr (FArray t)
            } -> FArray t
  deriving (Generic)

